const express = require('express')
const router = express.Router();
const qrcode = require('../Schema/Qrcode')
const logger = require("../logger")

router.post('/createQrcode',async(req,res)=>{
    try{
        if(req.body!==null){
            const qrcodeData = await qrcode.create(req.body)
            if(qrcodeData!==null){
            return res.status(201).json({
                message: 'QR Code data inserted',
                brandKitData: qrcodeData
            })
            }
        }
    }catch(err){
        logger.error(err)
        res.status(500).json({
            error: "internal server error"
        })
   
    }
})

router.post('/getAllQrcode',async(req,res)=>{
    try{
        const allQrcode = await qrcode.find({userId:req.body.userId})
        if (allQrcode !== null) {
            return res.status(200).json({
                message: 'QR Code fetch sucessfully...',
                data: allQrcode
            });
        } else {
            return res.status(401).json({
                error: 'QR Code data does not found...',
            });
        }
    }catch(err){
        logger.error(err)
        res.status(500).json({
            error: "internal server error"
        })
    }
})

router.delete('/deleteQRcode',async(req,res)=>{
  try{
    await qrcode.deleteOne({ _id: req.body.id })
    res.status(200).json({
        message: "deleted"
    })
  } catch(err){
    logger.error(err)
    res.status(500).json({
        error: "internal server error"
    })
  
  } 
})

module.exports = router