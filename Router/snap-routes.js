const snapRoutes = require("express").Router();

const passport = require("passport");
const CLIENT_URL = "http://localhost:3000/login-snap";
const token = require("../Schema/token");
const axios = require("axios");
const https = require("https");
const request = require("request");
require("mongoose");
const logger = require("../logger")

// login snapchat
snapRoutes.get("/login", function (req, res) {
  passport.authenticate("snapchat", { state: req.query.username })(req, res)

});

// redirect route
snapRoutes.get(
  "/redirect",
  passport.authenticate("snapchat"),
  async (req, res) => {

    const accessToken = req.user.accessToken;
    const refreshToken = req.user.refreshToken;
    const userId = req.user.userId
    const token_res = await token.find({ userId: req.user.userId });
    if (token_res.length > 0) {
      res.send("<script>window.close()</script>")
    } else {
      await token.create({
        accessToken,
        refreshToken,
        userId
      });
      res.send("<script>window.close()</script>");
    }
  }
);

// checking access token
snapRoutes.get("/check", async (re, rs) => {
  const token_res = await token.find({ userId: re.headers.userid });
  const text = re.headers.text;
  if (token_res.length > 0) {
    await axios
      .get("https://6cgnepgh03.execute-api.ap-south-1.amazonaws.com/latest/snapchat/get", {
        headers: {
          "Content-Type": "application/json",
          text: text ? text : "Hello",
          userid: re.headers.userid
        },
      })
      .then((res) => {
        rs.send(res.data);
      })
      .catch((error) => {
        logger.error(error)
        rs.status(404).send("error in get api");
      });
  } else {
    rs.status(404).send("error in check api");
  }
});

// getting data from accessToken ans text
snapRoutes.get("/get", async (re, rs) => {
  const text = re.headers.text;
  const token_res = await token.find({ userId: re.headers.userid });
  const accessToken = token_res[0].accessToken;

  const searchText = text ? text : "hello";

  // if text is null
  if (searchText == "") {
    searchText = "error";
  }
  var query =
    "query sticker($req:SearchStickersRequest!) {sticker {searchStickers(req:$req) {stickerResults {items {id}} bitmojiResults {items {pngLargeURL}} }}}";
  var variables = `{"req": {"searchStickersParams": {"searchText": "${searchText}", "numberResults": 50},"stickerUserContext": {}}}`;

  var body = '{"query" : "' + query + '", "variables": ' + variables + "}";

  const options = {
    hostname: "graph.snapchat.com",
    port: 443,
    path: "/graphql",
    method: "POST",
    headers: {
      Authorization: "Bearer " + accessToken,
      "Content-Type": "application/json",
    },
  };

  // response : outupt
  let output = "";
  const req = https.request(options, async (res) => {
    if (res.statusCode == 401) {
      const refresh_data = await axios.get(
        `https://6cgnepgh03.execute-api.ap-south-1.amazonaws.com/latest/snapchat/refresh?userId=${re.headers.userid}`
      );
      req.end();
      rs.send(refresh_data.data);
    }
    res.on("data", async (d) => {
      process.std;
      output += d;
    });
    res.on("error", () => {
      req.end();
      rs.send("error");
    });
    res.on("close", () => {
      req.end();
      if (output != "unauthorized") {
        rs.send(JSON.parse(output));
      }
    });
  });

  req.on("error", (error) => {
    rs.send("error");
    console.error("Hello");
    logger.error(error)
  });
  req.write(body);
  req.end();
});

// refreshing the token if token is not valid
snapRoutes.get("/refresh", async (req, res) => {
  const response = await token.find({ userId: req.query.userId });
  const id = await response[0]._id;
  const refreshToken = await response[0].refreshToken;

  // console.log(refreshToken);
  var SNAPCHAT_AUTH_ENDPOINT =
    "https://accounts.snapchat.com/accounts/oauth2/token";

  var authorizationHeader =
    process.env.clientID + ":" + process.env.clientSecret;
  var authorizationHeaderBase64 =
    Buffer.from(authorizationHeader).toString("base64");

  // Set headers
  var headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    Authorization: "Basic " + authorizationHeaderBase64,
  };

  var options = {
    url: SNAPCHAT_AUTH_ENDPOINT,
    method: "POST",
    headers: headers,
    form: {
      grant_type: "refresh_token",
      refresh_token: refreshToken,
    },
  };
  request(options, async function (error, response, body) {
    const response_body = await JSON.parse(response.body);
    await token.findByIdAndUpdate(id, {
      accessToken: response_body.access_token,
      refreshToken: response_body.refresh_token,
    });
    await axios
      .get("https://6cgnepgh03.execute-api.ap-south-1.amazonaws.com/latest/snapchat/get", {
        headers: { "Content-Type": "application/json", userid: req.query.userId },
      })
      .then((re) => {
        res.send(re.data);
      })
      .catch((error) => {
        logger.error(error)
        res.send("error");
      });
  });
});

module.exports = snapRoutes;
