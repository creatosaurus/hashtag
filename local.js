const app = require('./index')
const logger = require("./logger")

app.listen(4008, () => {
    logger.info("server listening on 4008")
})