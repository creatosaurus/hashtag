const mongoose = require('mongoose')
const schema = mongoose.Schema

const tags = new schema({
    organizationId: String,
    userId: String,
    userName: String,
    groupName: String,
    category: String,
    tags: [String]
}, { timestamps: true })

module.exports = mongoose.model("tags", tags)
