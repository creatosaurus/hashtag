const mongoose = require('mongoose')
const schema = mongoose.Schema

const categoryName = new schema({
    organizationId: String,
    userId: String,
    category: String
}, { timestamps: true })

module.exports = mongoose.model("Category Name", categoryName)
