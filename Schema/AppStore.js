const mongoose = require('mongoose')
const schema = mongoose.Schema;

const AppStore = new schema({
    userId:{
        type:String
    },
    appToken:{
        type:String
    },
    appType:{
        type:String
    }
})

module.exports = mongoose.model('AppStore',AppStore)