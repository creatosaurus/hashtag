const mongoose = require('mongoose')
const schema = mongoose.Schema;

const BrandKit = new schema({
    workspaceId: String,
    userId: String,
    title: String,
    logos: [{
        url: String,
        key: String,
        fileName: String
    }],
    colorsData: [{
        colors: []
    }],
    text: [{
        type: { type: String },
        fontName: String,
        fontWeight: String,
        fontSize: String,
        letterSpacing: Number,
        lineHeight: Number,
        align: String,
        list: String,
        case: String
    }]
})

module.exports = mongoose.model('BrandKit', BrandKit)

